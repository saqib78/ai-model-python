from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from uploads.core import views

urlpatterns = [
    # this is for second page
    # url(r'^adminn/$', views.adminn, name='adminn'),
    # this is for the first page
    url('personality_1/$', views.personality_1, name='personality_1'),
    url('personality_2/$', views.personality_2, name='personality_2'),
    url('interest_1/$', views.interest_1, name='interest_1'),
    url('interest_2/$', views.interest_2, name='interest_2'),
    url('degree_1/$', views.degree_1, name='degree_1'),
    url('analysis_1/$', views.analysis_1, name='analysis_1'),
    url('subcat_1/$', views.subcat_1, name='subcat_1')
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
