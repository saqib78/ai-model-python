from django.http import JsonResponse


from uploads.projectSVM.Interest_1 import *
from uploads.projectSVM.Interest_2 import *
from uploads.projectSVM.personality_1 import *
from uploads.projectSVM.personality_2 import *
from uploads.projectSVM.degree import *
from uploads.projectSVM.engineering import *
from uploads.projectSVM.analysis import *



def personality_1(request):
    b = (request.POST['v1'])
    c = (request.POST['v2'])
    h = (request.POST['v3'])
    o = (request.POST['v4'])
    p = (request.POST['v5'])
    t = (request.POST['v6'])
    result = personality1(b, c, h, o, p, t)
    return JsonResponse((result), safe=False)


def personality_2(request):
    b = (request.POST['v1'])
    c = (request.POST['v2'])
    h = (request.POST['v3'])
    o = (request.POST['v4'])
    p = (request.POST['v5'])
    t = (request.POST['v6'])
    result = personality2(b, c, h, o, p, t)
    return JsonResponse((result), safe=False)


def interest_1(request):
    cm = (request.POST['v1'])
    cr = (request.POST['v2'])
    f = (request.POST['v3'])
    eng = (request.POST['v4'])
    sci = (request.POST['v5'])
    bus = (request.POST['v6'])
    swork = (request.POST['v7'])
    defen = (request.POST['v8'])
    hosp = (request.POST['v9'])
    social = (request.POST['v10'])
    result = interest1(cm, cr, f, eng, sci, bus, swork, defen, hosp, social)
    return JsonResponse((result), safe=False)


def interest_2(request):
    cm = (request.POST['v1'])
    cr = (request.POST['v2'])
    f = (request.POST['v3'])
    eng = (request.POST['v4'])
    sci = (request.POST['v5'])
    bus = (request.POST['v6'])
    swork = (request.POST['v7'])
    defen = (request.POST['v8'])
    hosp = (request.POST['v9'])
    social = (request.POST['v10'])
    result = interest2(cm, cr, f, eng, sci, bus, swork, defen, hosp, social)
    return JsonResponse((result), safe=False)


def degree_1(request):
    Numerical_Reasoning = (request.POST['v1'])
    Mechanical = (request.POST['v2'])
    Deductive = (request.POST['v3'])
    Verbal = (request.POST['v4'])
    Inductive = (request.POST['v5'])
    Error_Checking = (request.POST['v6'])
    Building = (request.POST['v7'])
    Thinking = (request.POST['v8'])
    Creative1 = (request.POST['v9'])
    Helping = (request.POST['v10'])
    Persuading = (request.POST['v11'])
    Organizing = (request.POST['v12'])
    Comm_Media = (request.POST['v13'])
    Creative2 = (request.POST['v14'])
    Fashion = (request.POST['v15'])
    Engineering = (request.POST['v16'])
    Sci_Tech = (request.POST['v17'])
    Bus_Mngt = (request.POST['v18'])
    Social_Work = (request.POST['v19'])
    Defense = (request.POST['v20'])
    Hospitality = (request.POST['v21'])
    Social_Sciences = (request.POST['v22'])

    result = degree1(Numerical_Reasoning, Mechanical, Deductive, Verbal, Inductive, Error_Checking, Building, Thinking,
                    Creative1,Helping, Persuading, Organizing, Comm_Media, Creative2, Fashion, Engineering, Sci_Tech, Bus_Mngt,
                    Social_Work,Defense, Hospitality, Social_Sciences)
    return JsonResponse((result), safe=False)



def analysis_1(request):
    Numerical_Reasoning = (request.POST['v1'])
    Mechanical = (request.POST['v2'])
    Deductive = (request.POST['v3'])
    Verbal = (request.POST['v4'])
    Inductive = (request.POST['v5'])
    Error_Checking = (request.POST['v6'])
    Building = (request.POST['v7'])
    Thinking = (request.POST['v8'])
    Creative1 = (request.POST['v9'])
    Helping = (request.POST['v10'])
    Persuading = (request.POST['v11'])
    Organizing = (request.POST['v12'])
    Comm_Media = (request.POST['v13'])
    Creative2 = (request.POST['v14'])
    Fashion = (request.POST['v15'])
    Engineering = (request.POST['v16'])
    Sci_Tech = (request.POST['v17'])
    Bus_Mngt = (request.POST['v18'])
    Social_Work = (request.POST['v19'])
    Defense = (request.POST['v20'])
    Hospitality = (request.POST['v21'])
    Social_Sciences = (request.POST['v22'])

    results = analysis1(Numerical_Reasoning, Mechanical, Deductive, Verbal, Inductive, Error_Checking, Building, Thinking,
                    Creative1,Helping, Persuading, Organizing, Comm_Media, Creative2, Fashion, Engineering, Sci_Tech, Bus_Mngt,
                    Social_Work,Defense, Hospitality, Social_Sciences)
    return JsonResponse(results, safe=False)

def subcat_1(request):
    Numerical_Reasoning = (request.POST['v1'])
    Mechanical = (request.POST['v2'])
    Deductive = (request.POST['v3'])
    Verbal = (request.POST['v4'])
    Inductive = (request.POST['v5'])
    Error_Checking = (request.POST['v6'])
    Building = (request.POST['v7'])
    Thinking = (request.POST['v8'])
    Creative1 = (request.POST['v9'])
    Helping = (request.POST['v10'])
    Persuading = (request.POST['v11'])
    Organizing = (request.POST['v12'])
    Comm_Media = (request.POST['v13'])
    Creative2 = (request.POST['v14'])
    Fashion = (request.POST['v15'])
    Engineering = (request.POST['v16'])
    Sci_Tech = (request.POST['v17'])
    Bus_Mngt = (request.POST['v18'])
    Social_Work = (request.POST['v19'])
    Defense = (request.POST['v20'])
    Hospitality = (request.POST['v21'])
    Social_Sciences = (request.POST['v22'])

    result = engineering_subcat1(Numerical_Reasoning, Mechanical, Deductive, Verbal, Inductive, Error_Checking, Building, Thinking,
                    Creative1,Helping, Persuading, Organizing, Comm_Media, Creative2, Fashion, Engineering, Sci_Tech, Bus_Mngt,
                    Social_Work,Defense, Hospitality, Social_Sciences)
    return JsonResponse((result), safe=False)