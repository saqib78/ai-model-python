import nltk
import pickle
import pandas as pd


with open("uploads/projectSVM/interest1.pkl", 'rb') as file:
    SVM = pickle.load(file)


def interest1(CommMedia, Creative, Fashion, Engineering, SciTech, BusMngt, SocialWork, Defense, Hospitality,
              SocialSciences):
    # print(Building)
    input = {'Comm & Media': [CommMedia], 'Creative': [Creative], 'Fashion': [Fashion], 'Engineering': [Engineering],
             'Sci & Tech': [SciTech], 'Bus & Mngt': [BusMngt],
             'Social Work': [SocialWork], 'Defense': [Defense], 'Hospitality': [Hospitality],
             'Social Sciences': [SocialSciences]}
    x = pd.DataFrame(input, columns=['Comm & Media', 'Creative', 'Fashion', 'Engineering', 'Sci & Tech', 'Bus & Mngt',
                                     'Social Work', 'Defense', 'Hospitality', 'Social Sciences'])
    # print(x)
    p1 = SVM.predict(x)
    return {'interest1':p1[0]}
