import pickle
from typing import List, Any, Union, Set, Tuple

import pandas as pd


################## defination of degree call ##################

def engineering_subcat1(Numerical_Reasoning, Mechanical, Deductive, Verbal, Inductive, Error_Checking, Building, Thinking,
            Creative1,
            Helping, Persuading, Organizing, Comm_Media, Creative2, Fashion, Engineering, Sci_Tech, Bus_Mngt,
            Social_Work,
            Defense, Hospitality, Social_Sciences):
    input = {'Numerical_Reasoning': [Numerical_Reasoning], 'Mechanical': [Mechanical], 'Deductive': [Deductive],
             'Verbal': [Verbal], 'Inductive': [Inductive], 'Error_Checking': [Error_Checking], 'Building': [Building],
             'Thinking': [Thinking],
             'Creative1': [Creative1], 'Helping': [Helping], 'Persuading': [Persuading], 'Organizing': [Organizing],
             'Comm_Media': [Comm_Media],
             'Creative2': [Creative2], 'Fashion': [Fashion], 'Engineering': [Engineering], 'Sci_Tech': [Sci_Tech],
             'Bus_Mngt': [Bus_Mngt], 'Social_Work': [Social_Work],
             'Defense': [Defense], 'Hospitality': [Hospitality], 'Social_Sciences': [Social_Sciences]}

    x = pd.DataFrame(input, columns=['Numerical_Reasoning', 'Mechanical', 'Deductive',
                                     'Verbal', 'Inductive', 'Error_Checking', 'Building', 'Thinking',
                                     'Creative1', 'Helping', 'Persuading', 'Organizing', 'Comm_Media',
                                     'Creative2', 'Fashion', 'Engineering', 'Sci_Tech', 'Bus_Mngt', 'Social_Work',
                                     'Defense', 'Hospitality', 'Social_Sciences'])

    # x=x1.iloc[:,1:22]


    file1 = open("uploads/projectSVM/engineering.pkl", 'rb')
    svm1 = pickle.load(file1)
    result = svm1.predict(x)
    file1.close()

    return {'subcat':str(result[0]) }