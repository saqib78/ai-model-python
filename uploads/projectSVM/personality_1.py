import pickle
import pandas as pd

with open("uploads/projectSVM/personality_1.pkl", 'rb') as file:
    SVM = pickle.load(file)


def personality1(Building, Creating, Helping, Organizing, Persuading, Thinking):
    # print(Building)
    input = {'Building': [Building], 'Creating': [Creating], 'Helping': [Helping], 'Organizing': [Organizing],
             'Persuading': [Persuading], 'Thinking': [Thinking]}
    x = pd.DataFrame(input, columns=['Building', 'Creating', 'Helping', 'Organizing', 'Persuading', 'Thinking'])
    # print(x)
    x = x.iloc[:, 1:6]
    p1 = SVM.predict(x)
    return {'personality1':p1[0]}
