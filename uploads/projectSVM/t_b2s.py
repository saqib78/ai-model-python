import itertools
import pickle
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn import svm
from sklearn.metrics import confusion_matrix, accuracy_score, classification_report
from sklearn.model_selection import GridSearchCV
from sklearn.utils import shuffle

dataset = pd.read_excel('dataset.xlsx', sheet_name='b2s')
# print(dataset.head())
dataset = dataset.dropna()
dataset = shuffle(dataset)
# print(dataset.groupby('Second Dominant Personality').count())

# Separate report and labels

# report = dataset.iloc[:, 29]
report = dataset.iloc[:, 6:28]
# print(report.head())
labels = dataset.iloc[:, 29]

def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    # plt.show()


#
#
# #tuning
def svc_param_selection(X, y, nfolds):
    Cs = [0.001, 0.01, 0.1, 1, 10]
    gammas = [0.001, 0.01, 0.1, 1]
    param_grid = {'C': Cs, 'gamma': gammas}
    grid_search = GridSearchCV(svm.SVC(kernel='rbf'), param_grid, cv=nfolds)
    grid_search.fit(X, y)
    grid_search.best_params_
    return grid_search.best_params_


#
# # Train & Test the SVM Classifier
#
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC

#
X_train, X_test, y_train, y_test = train_test_split(report, labels, test_size=0.2, stratify=labels, train_size=0.8)
print(y_test.groupby(y_test.iloc[:]).count())
# print(svc_param_selection(report, labels, nfolds=5))
SVM = SVC(kernel='rbf', gamma=0.1, C=5000)
# 0.001 10
# 0.01 5000
SVM.fit(X_train, y_train)
print('Score of training is = {:.2f}%'.format(SVM.score(X_train, y_train) * 100))
#
# #Test the model
#
y_pred = SVM.predict(X_test)
cm = confusion_matrix(y_test, y_pred)
cm_plot_labels = ['Engineering', 'Natural / Applied Sciences']
plot_confusion_matrix(cm, cm_plot_labels, title='confusion_matrix')
# Print classification report (Precision, Recall, F1)
target_names = ['Engineering / Computers', 'Natural / Applied Sciences']
print(classification_report(y_test, y_pred, target_names=target_names))
#
# #Print the Acuraccy of the model
Accuracy_Score = accuracy_score(y_test, y_pred)
print('Test Accuracy score is = {:.2f}%'.format(Accuracy_Score * 100))

with open("b2s.pkl", 'wb') as file:
    pickle.dump(SVM, file)
