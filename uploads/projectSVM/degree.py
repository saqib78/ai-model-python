import pickle
from typing import List, Any, Union, Set, Tuple

import pandas as pd


################## defination of degree call ##################

def degree1(Numerical_Reasoning, Mechanical, Deductive, Verbal, Inductive, Error_Checking, Building, Thinking,
            Creative1,
            Helping, Persuading, Organizing, Comm_Media, Creative2, Fashion, Engineering, Sci_Tech, Bus_Mngt,
            Social_Work,
            Defense, Hospitality, Social_Sciences):
    input = {'Numerical_Reasoning': [Numerical_Reasoning], 'Mechanical': [Mechanical], 'Deductive': [Deductive],
             'Verbal': [Verbal], 'Inductive': [Inductive], 'Error_Checking': [Error_Checking], 'Building': [Building],
             'Thinking': [Thinking],
             'Creative1': [Creative1], 'Helping': [Helping], 'Persuading': [Persuading], 'Organizing': [Organizing],
             'Comm_Media': [Comm_Media],
             'Creative2': [Creative2], 'Fashion': [Fashion], 'Engineering': [Engineering], 'Sci_Tech': [Sci_Tech],
             'Bus_Mngt': [Bus_Mngt], 'Social_Work': [Social_Work],
             'Defense': [Defense], 'Hospitality': [Hospitality], 'Social_Sciences': [Social_Sciences]}

    x = pd.DataFrame(input, columns=['Numerical_Reasoning', 'Mechanical', 'Deductive',
                                     'Verbal', 'Inductive', 'Error_Checking', 'Building', 'Thinking',
                                     'Creative1', 'Helping', 'Persuading', 'Organizing', 'Comm_Media',
                                     'Creative2', 'Fashion', 'Engineering', 'Sci_Tech', 'Bus_Mngt', 'Social_Work',
                                     'Defense', 'Hospitality', 'Social_Sciences'])

    # x=x1.iloc[:,1:22]

    ################## match b2c ##################
    file1 = open("uploads/projectSVM/b2c.pkl", 'rb')
    svm1 = pickle.load(file1)
    result1 = svm1.predict(x)
    file1.close()

    ################## match b2e ##################
    file1 = open("uploads/projectSVM/b2e.pkl", 'rb')
    svm1 = pickle.load(file1)
    result2 = svm1.predict(x)
    file1.close()

    ################## match b2n ##################
    file1 = open("uploads/projectSVM/b2n.pkl", 'rb')
    svm1 = pickle.load(file1)
    result3 = svm1.predict(x)
    file1.close()

    ################## match b2s ##################
    file1 = open("uploads/projectSVM/b2s.pkl", 'rb')
    svm1 = pickle.load(file1)
    result4 = svm1.predict(x)
    file1.close()

    ################## match c2e ##################
    file1 = open("uploads/projectSVM/c2e.pkl", 'rb')
    svm1 = pickle.load(file1)
    result5 = svm1.predict(x)
    file1.close()

    ################## match c2n ##################
    file1 = open("uploads/projectSVM/c2n.pkl", 'rb')
    svm1 = pickle.load(file1)
    result6 = svm1.predict(x)
    file1.close()

    ################## match c2s ##################
    file1 = open("uploads/projectSVM/c2s.pkl", 'rb')
    svm1 = pickle.load(file1)
    result7 = svm1.predict(x)
    file1.close()

    ################## match e2n ##################
    file1 = open("uploads/projectSVM/e2n.pkl", 'rb')
    svm1 = pickle.load(file1)
    result8 = svm1.predict(x)
    file1.close()

    ################## match e2s ##################
    file1 = open("uploads/projectSVM/e2s.pkl", 'rb')
    svm1 = pickle.load(file1)
    result9 = svm1.predict(x)
    file1.close()

    ################## match n2s ##################
    file1 = open("uploads/projectSVM/n2s.pkl", 'rb')
    svm1 = pickle.load(file1)
    result10 = svm1.predict(x)
    file1.close()

    ################## match b2d ##################
    file1 = open("uploads/projectSVM/b2d.pkl", 'rb')
    svm1 = pickle.load(file1)
    result11 = svm1.predict(x)
    file1.close()

    ################## match c2d ##################
    file1 = open("uploads/projectSVM/c2d.pkl", 'rb')
    svm1 = pickle.load(file1)
    result12 = svm1.predict(x)
    file1.close()

    ################## match e2d ##################
    file1 = open("uploads/projectSVM/e2d.pkl", 'rb')
    svm1 = pickle.load(file1)
    result13 = svm1.predict(x)
    file1.close()

    ################## match n2d ##################
    file1 = open("uploads/projectSVM/n2d.pkl", 'rb')
    svm1 = pickle.load(file1)
    result14 = svm1.predict(x)
    file1.close()

    ################## match s2d ##################
    file1 = open("uploads/projectSVM/s2d.pkl", 'rb')
    svm1 = pickle.load(file1)
    result15 = svm1.predict(x)
    file1.close()

    result = [result1[0], result2[0], result3[0], result4[0], result5[0], result6[0], result7[0],
              result8[0], result9[0], result10[0],result11[0], result12[0], result13[0], result14[0], result15[0]]

#    index1 = [0, 1, 2, 3, 4, 5, 6, 7, 8]
#    index2 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
#    cnt = [0, 0, 0, 0, 0, 0, 0, 0, 0]

    df = pd.DataFrame({'output': result})
    out1 = df['output'].value_counts()

    # out2=out1.to_frame()

    temp = out1[:1].keys()
    out2= temp.to_frame()
    deg1 = out2.iloc[0,0]

    temp = out1[1:2].keys()
    out2= temp.to_frame()
    deg2 = out2.iloc[0,0]

    return {'degree1':str(deg1), 'degree2':str(deg2)}