from uploads.projectSVM.Interest_1 import *
from uploads.projectSVM.Interest_2 import *
from uploads.projectSVM.degree import *
from uploads.projectSVM.personality_1 import *
from uploads.projectSVM.personality_2 import *
from uploads.projectSVM.engineering import *


def analysis1(Numerical_Reasoning, Mechanical, Deductive, Verbal, Inductive, Error_Checking, Building, Thinking,
              Creative1,
              Helping, Persuading, Organizing, Comm_Media, Creative2, Fashion, Engineering, Sci_Tech, Bus_Mngt,
              Social_Work,
              Defense, Hospitality, Social_Sciences):
    input = {'Numerical_Reasoning': [Numerical_Reasoning], 'Mechanical': [Mechanical], 'Deductive': [Deductive],
             'Verbal': [Verbal], 'Inductive': [Inductive], 'Error_Checking': [Error_Checking], 'Building': [Building],
             'Thinking': [Thinking],
             'Creative1': [Creative1], 'Helping': [Helping], 'Persuading': [Persuading], 'Organizing': [Organizing],
             'Comm_Media': [Comm_Media],
             'Creative2': [Creative2], 'Fashion': [Fashion], 'Engineering': [Engineering], 'Sci_Tech': [Sci_Tech],
             'Bus_Mngt': [Bus_Mngt], 'Social_Work': [Social_Work],
             'Defense': [Defense], 'Hospitality': [Hospitality], 'Social_Sciences': [Social_Sciences]}

    x = pd.DataFrame(input, columns=['Numerical_Reasoning', 'Mechanical', 'Deductive',
                                     'Verbal', 'Inductive', 'Error_Checking', 'Building', 'Thinking',
                                     'Creative1', 'Helping', 'Persuading', 'Organizing', 'Comm_Media',
                                     'Creative2', 'Fashion', 'Engineering', 'Sci_Tech', 'Bus_Mngt', 'Social_Work',
                                     'Defense', 'Hospitality', 'Social_Sciences'])
    # x = x1.iloc[:, 1:22]
    ############### predict degree
    e = x.to_numpy()
    deg = degree1(e[0, 0], e[0, 1], e[0, 2], e[0, 3], e[0, 4], e[0, 5],
                  e[0, 6], e[0, 7], e[0, 8], e[0, 9], e[0, 10], e[0, 11],
                  e[0, 12], e[0, 13], e[0, 14], e[0, 15], e[0, 16], e[0, 17],
                  e[0, 18], e[0, 19], e[0, 20], e[0, 21])

    ############# predict personality

    d = x.iloc[:, 6:12]
    e = d.to_numpy()

    per1 = personality1(e[0, 0], e[0, 1], e[0, 2], e[0, 3], e[0, 4], e[0, 5])
    per2 = personality2(e[0, 0], e[0, 1], e[0, 2], e[0, 3], e[0, 4], e[0, 5])

    d = x.iloc[:, 12:22]
    e = d.to_numpy()

    int1 = interest1(e[0, 0], e[0, 1], e[0, 2], e[0, 3], e[0, 4], e[0, 5], e[0, 6], e[0, 7], e[0, 8], e[0, 9])
    int2 = interest2(e[0, 0], e[0, 1], e[0, 2], e[0, 3], e[0, 4], e[0, 5], e[0, 6], e[0, 7], e[0, 8], e[0, 9])

    e = x.to_numpy()
    subcat = engineering_subcat1(e[0, 0], e[0, 1], e[0, 2], e[0, 3], e[0, 4], e[0, 5], e[0, 6], e[0, 7], e[0, 8], e[0, 9], e[0, 10], e[0, 11], e[0, 12], e[0, 13], e[0, 14], e[0, 15], e[0, 16], e[0, 17],e[0, 18], e[0, 19], e[0, 20], e[0, 21])

    return {'personality1': per1['personality1'], 'personality2':per2['personality2'], 'interest1':int1['interest1'], 'interest2':int2['interest2'], 'degree1':deg['degree1'],'degree2':deg['degree2'],'engineering_subcat':subcat['subcat']}
